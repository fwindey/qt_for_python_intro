from PySide2 import QtWidgets
from PySide2.QtCore import Signal
from PySide2.QtGui import Qt, QIcon

weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

priorities = ["High", "Medium", "Low"]
priority_bg_colors = ["#CC0000", "#AADD00", "#22DD00"]
priority_text_colors = ["#FFFFFF", "#000000", "#000000"]


class AddAgendaItemDialog(QtWidgets.QDialog):

    on_dialog_finished = Signal(str, str, str)

    def __init__(self, parent):
        super().__init__(parent)

        self.setWindowTitle("Add agenda item")

        self._layout = QtWidgets.QVBoxLayout()
        self.setLayout(self._layout)

        self._item_description = QtWidgets.QTextEdit()
        self._item_description.setPlaceholderText("item description")
        self._layout.addWidget(self._item_description)

        self._weekday_combo = QtWidgets.QComboBox()
        self._weekday_combo.addItems(weekdays)
        self._layout.addWidget(self._weekday_combo)

        self._priority_combo = QtWidgets.QComboBox()
        self._priority_combo.addItems(priorities)
        self._layout.addWidget(self._priority_combo)

        self._ok_button = QtWidgets.QPushButton("Ok")
        self._ok_button.clicked.connect(self._on_ok_clicked)
        self._layout.addWidget(self._ok_button)

    def _on_ok_clicked(self):
        self.on_dialog_finished.emit(self._item_description.toPlainText(), self._weekday_combo.currentText(),
                                     self._priority_combo.currentText())
        self.close()


class AgendaItemWidget(QtWidgets.QWidget):

    def __init__(self, parent, description):
        super().__init__(parent)

        description_label = QtWidgets.QLabel(description)
        description_label.setAlignment(Qt.AlignTop)
        description_label.setStyleSheet("""
            background-color: #DDDDDD;
            padding: 5px;
        """)
        delete_button = QtWidgets.QPushButton("delete")
        delete_button.clicked.connect(self._on_delete_clicked)
        item_layout = QtWidgets.QVBoxLayout()
        self.setLayout(item_layout)
        item_layout.addWidget(description_label)
        item_layout.addWidget(delete_button)
        item_layout.setContentsMargins(0, 0, 0, 0)

    def _on_delete_clicked(self):
        self.close()


class AgendaWidget(QtWidgets.QWidget):

    def __init__(self, parent):
        super().__init__(parent)

        # define main layout
        self._main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(self._main_layout)

        # add the toolbar
        tool_bar = self._construct_tool_bar()
        self._main_layout.addWidget(tool_bar)

        # define the agenda layout
        self._agenda_layout = QtWidgets.QGridLayout()
        self._main_layout.addLayout(self._agenda_layout)

        self._agenda_layout.addWidget(QtWidgets.QWidget(), 0, 0)

        # add weekday labels
        column = 1
        for day in weekdays:
            day_label = QtWidgets.QLabel(day)
            day_label.setAlignment(Qt.AlignCenter)
            day_label.setStyleSheet(f"""
                QLabel{{
                    background-color: #DDDDDD;
                    text-align: center;
                    font-size: 25px;
                }}
            """)
            self._agenda_layout.addWidget(day_label, 0, column)
            column += 1

        # add priority labels
        row = 1
        for priority, priority_color, text_color in zip(priorities, priority_bg_colors, priority_text_colors):
            priority_label = QtWidgets.QLabel(priority)
            priority_label.setStyleSheet(f"""
                QLabel{{
                    background-color : {priority_color};
                    color : {text_color};
                    padding: 10px;
                    font-size: 25px;
                }}
            """)
            self._agenda_layout.addWidget(priority_label, row, 0)
            row += 1

        self._add_item_dialog = AddAgendaItemDialog(self)
        self._add_item_dialog.on_dialog_finished.connect(self._on_add_agenda_dialog_finished)

    def _construct_tool_bar(self):
        tool_bar = QtWidgets.QToolBar(self)
        add_agenda_item_action = tool_bar.addAction("Add agenda item") #type: QtWidgets.QAction
        add_agenda_item_action.setIcon(QIcon("./icons/add_calender.png"))
        add_agenda_item_action.triggered.connect(self._on_add_agenda_item)
        return tool_bar

    def _on_add_agenda_item(self):
        self._add_item_dialog.show()

    def _on_add_agenda_dialog_finished(self, description, weekday, priority):
        weekday_index = weekdays.index(weekday)
        priority_index = priorities.index(priority)

        agenda_item_widget = AgendaItemWidget(self, description)
        self._agenda_layout.addWidget(agenda_item_widget, priority_index + 1, weekday_index + 1)
