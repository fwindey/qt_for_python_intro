# Introduction project for GUI programming using Qt for Python

This project builds a basic agenda app that allows adding todo items with a given priority.
It demonstrates the use of the Qt for Python api to build a desktop GUI application.

# Running the project

Create a conda environment, e.g. using [Miniforge](https://github.com/conda-forge/miniforge), and install the Pyside2 package via:

```bash
conda install pyside2
```

Run the app using ``main_dialog.py`` as entry point:

```bash
python main_dialog.py
```
