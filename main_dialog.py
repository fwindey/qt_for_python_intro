from PySide2 import QtWidgets
from agenda_widget import AgendaWidget


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Qt intro")

        # construct the main menu bar
        self._construct_menu_bar()

        agenda_widget = AgendaWidget(self)
        self.setCentralWidget(agenda_widget)

        # set size of main app to 60 percent of the screen size
        self.resize(QtWidgets.QDesktopWidget().availableGeometry().size() * 0.6)

    def _construct_menu_bar(self):
        menu_bar = QtWidgets.QMenuBar(self)

        help_menu = menu_bar.addMenu("Help")
        about_action = help_menu.addAction("About") #type: QtWidgets.QAction
        about_action.triggered.connect(self._on_toolbar_about_clicked)
        self.setMenuBar(menu_bar)

    def _on_toolbar_about_clicked(self):
        QtWidgets.QMessageBox.about(self, "About", "Qt intro example application")


if __name__ == '__main__':
    app = QtWidgets.QApplication()

    main = MainWindow()
    main.show()

    app.exec_()
